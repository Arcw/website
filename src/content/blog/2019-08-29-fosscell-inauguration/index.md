---
path: /blog/inauguration
date: "2019-08-28"
cover: "./light.jpg"
tag: "Inauguration"
author: "Athul Krishnan"
name: "Athul Krishnan P M"
title: "Inauguration"
desc: "About the inauguration of our cell"
---



It was not so long I have heard about Free and Open Source Software .One of my close friends was a regular Linux user, so it was very easy for me to enter the world of FOSS and Linux . The first time I sat on a Linux distro, what attracted me the most was the adventurous ride of trying out different things and the ability to customize whatever I want without ever worrying about whether a virus might hit my computer. If it crashes? Well boot it up again and everything is back to normal .The awesome part about all of this was everything and I mean everything was available online.
 
So after a lot of discussions a few students in our peer group  planned to establish a FOSS community in our college .There were lot of difficulties to start a new cell in our college. First of all there should be a teacher staff to coordinate the cell and the effort of students of all other department other than Computer Science was required .Thankfully we’ve got Mr Syam Sankar as our staff coordinator and several students from other department also to join our community.And the hardwork of many members helped our FOSS cell to get initiated in our college.

![a man looking at mountain](./light.jpg)

The next step was to conduct an inaugural ceremony for our newly formed cell.
So the inauguration of our FOSS cell was planned  and organised.I got a chance to participate actively in the organisation of the event .Even though i was a member of FOSS cell i had only a little knowledge about necessity of using FOSS. This inaugural ceremony and a session named ‘Privacy Yatra’ changed my attitude towards Free and Open Source Software.The inaugural ceremony was planned to start on afternoon 3 PM. There were lot of works to do to conduct the function.The FOSS 

Cell members actively participated in the management of the function.Our staff coordinator Mr Syam Sankar and student coordinator Abhinav Krishna done all the necessary activities for the inauguration.And the inauguration was started on  right time.Our chief guest was Mr  Praveen Arimbrathodiyil widely known as Pirate Praveen, A famous debian software developer who had made great contributions to the Linux platform.

I was in an excitement.It was for the first time i got an oppurtunity to participate actively in a Cell in our college other than the curricular activities.The Principal just mentioned about the Contributions of Richard Mathew Stallman,The Founder of Free Software Foundation in the Free and open source software community. This is when I realized that the FOSS is so much popular among the people who engaged in technical field.I also got to know about the usage of FOSS in students from depatments like Mechanical,Civil etc..


![privacy yatra](./p.JPG)


Shortly after Pirate Praveen inugurated the ceremony by lightening the lamp.After the inauguration the sessions of Privacy Yatra was handled by Mr Praveen and his wife Mrs Sruthi.In the starting of the session i just thought that it was about the privacy in cyber world.But his talks was mainly on how social media shapes our daily lives.
I was under the impression that the social media applications  are in our control and our private data is safe and secure inside it.


As the session was proceeding further I began to understand the reality about it.Social media had already become the strongest element in the world .He also mentioned about the election campign of Donald Trump in US president election and How it was mainly concentrated on Facebook.In that election there were lot of challenges for Trump to win the election.But Facebook  created a positive impact for trump in their social network and surprisingly Trump wins the election.I just thought that how can it be  happening?I was terrified by the impact of social media on our thought process.

![group](./group.JPG)

This is only just one case .There are lot of examples in the world where a social media decides what we think.He said that only possible solution for this controversy is to make emphasis on FOSS and develop FOSS culture on social networking sites.He also mentioned about the FOSS alternatives for the popular social media sites .The main advantage of FOSS social media services is that we are able to understand to what extend our data is secured.This session changed my attitude towards social media and after that i tried to use and propagate the FOSS alternatives for social media applications.So this inauguration day was one of the special and memorable one in my college days.


